<?php

define(STORE_FILE, 'store.json');

if (!isset($_GET['redirect_url'])) {
	header('HTTP/1.1 400 Bad Request');
	echo 'Redirect Url not found!';
	return;
}

$redirectUrl = rawurldecode($_GET['redirect_url']);

if (!filter_var($redirectUrl, FILTER_VALIDATE_URL)) {
	header('HTTP/1.1 400 Bad Request');
	echo 'Redirect Url incorrect!';
	return;
}

$store = json_decode(file_get_contents(STORE_FILE), true);

$store['redirectCount'] += 1;
$storeResource = fopen(STORE_FILE, 'w') or die('Unable to open file!');
fwrite($storeResource, json_encode($store));
fclose($storeResource);

if (
	$store['redirectOffset'] < $store['redirectCount'] &&
	rand(0, 100) <= $store['probability']
) {
	$urlParts = parse_url($redirectUrl);
	$urlQuery = $urlParts['query'];
	parse_str($urlQuery, $queryParams);

	foreach ($store['removedParams'] as $param) {
		unset($queryParams[$param]);
	}

	$redirectUrl = $urlParts['scheme']
		. '://'
		. $urlParts['host']
		. '/?'
		. http_build_query($queryParams);
}

header('Location: ' . $redirectUrl);